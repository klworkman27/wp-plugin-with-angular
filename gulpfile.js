// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const inject = require('gulp-inject');
var replace = require('gulp-replace');

// File paths
const files = { 
    scssPath: 'app/scss/**/*.scss',
    jsPath: 'app/js/**/*.js'
}

// Sass task: compiles the style.scss file into style.css
function scssTask(){    
    return src(files.scssPath)
        .pipe(sourcemaps.init()) // initialize sourcemaps first
        .pipe(sass()) // compile SCSS to CSS
        .pipe(postcss([ autoprefixer(), cssnano() ])) // PostCSS plugins
        .pipe(sourcemaps.write('.')) // write sourcemaps file in current directory
        .pipe(dest('app/css')
    ); // put final CSS in dist folder
}

// JS task: concatenates and uglifies JS files to script.js
function jsTask(){
    return src([
        files.jsPath, '!app/js/all.js'
        ])
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(dest('app/js')
    );
}

// Watch task: watch SCSS and JS files for changes
// If any change, run scss and js tasks simultaneously
function watchTask(){
    watch([files.scssPath, files.jsPath, '!app/js/all.js'], 
        parallel(scssTask, jsTask)
    );
}

function moveAppToProdTask(){
    return src(['app/js/**/*.*', 
                'app/css/**/*.*',
                'app/php/**/*.*' ], 
                { base: './' })
        .pipe(dest('angular-plugin-boilerplate')
    );
}

function moveAngularFilesToProdTask(){
    return src(['frontendAngular/dist/**/*.*'], 
                { base: './frontendAngular/dist/' })
        .pipe(dest('angular-plugin-boilerplate')
    );
}

function movePluginFilesToProdTask(){
    return src(['angular-plugin-boilerplate.php', 'controllers/**/*.*'], { base: './' })
        .pipe(dest('angular-plugin-boilerplate')
    );
}
// end of moving to productions functions

// Switch to PROD functions
function angular_plugin_boilerplate_to_prod(){
    return src(['angular-plugin-boilerplate.php'], {base: "./"})
    .pipe(replace("'frontendAngular/dist/frontendAngular/index.html'", "'frontendAngular/index.html'"))
    .pipe(dest("./"));

}

function index_to_prod(){
    return src(['frontendAngular/src/index.html'], {base: "./"})
    .pipe(replace('"/wp-content/plugins/angular-plugin-boilerplate/frontendAngular/dist/frontendAngular/"', '"/wp-content/plugins/angular-plugin-boilerplate/frontendAngular/"'))
    .pipe(dest("./"));
}

function services_to_prod(){
    return src(['frontendAngular/src/app/components/subscriber/subscriber.service.ts', 'frontendAngular/src/app/components/register/register.service.ts'], {base: "./"})
    .pipe(replace('"../../../controllers"', '"../controllers"'))
    .pipe(dest("./"));

}
// end of Switch to PROD functions

// Switch to DEV functions
function angular_plugin_boilerplate_to_dev(){
    return src(['angular-plugin-boilerplate.php'], {base: "./"})
    .pipe(replace("'frontendAngular/index.html'", "'frontendAngular/dist/frontendAngular/index.html'"))
    .pipe(dest("./"));
}

function index_to_dev(){
    return src(['frontendAngular/src/index.html'], {base: "./"})
    .pipe(replace('"/wp-content/plugins/angular-plugin-boilerplate/frontendAngular/"', '"/wp-content/plugins/angular-plugin-boilerplate/frontendAngular/dist/frontendAngular/"'))
    .pipe(dest("./"));
}

function services_to_dev(){
    return src(['frontendAngular/src/app/components/subscriber/subscriber.service.ts', 'frontendAngular/src/app/components/register/register.service.ts'], {base: "./"})
    .pipe(replace('"../controllers"', '"../../../controllers"'))
    .pipe(dest("./"));
}
// end of Switch to DEV functions

// Export the default Gulp task so it can be run
// Runs the scss and js tasks simultaneously
// then watch task
exports.default = series(
    parallel(jsTask), 
    watchTask
);

// Updates files to use Production locations.  
exports.switchToProd = series(
    parallel( angular_plugin_boilerplate_to_prod, index_to_prod, services_to_prod )
);

// Updates files to use Dev locations.  
exports.switchToDev = series(
    parallel( angular_plugin_boilerplate_to_dev, index_to_dev, services_to_dev )
);

// Exports the build Gulp task.  
// moves all the files to the production folder
exports.build = series(
    // parallel(jsTask), 
    parallel( moveAppToProdTask, movePluginFilesToProdTask, moveAngularFilesToProdTask)
);