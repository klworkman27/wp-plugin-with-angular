<?php

class Subscribers
{	
    public $table;

	public function __construct() {
        global $wpdb;
        $this->table = $wpdb->prefix."angular_boilerplate";
    }

    public function add($data){ 
        global $wpdb;
        $variables = (array)$data;
        $variables['date_added'] = date("Y-m-d H:i:s");

        $alreadyExists = $this->exists('email', $data->email);

        if($alreadyExists == false){
            $wpdb->insert( 
                $this->table, 
                $variables
            );

            $variables['id'] = $wpdb->insert_id;
            echo json_encode(array( 
                        'response' => array(
                            'success' => true,
                            'message' => 'user was added'
                        ),
                        'user' => $variables ));

        } else {
            echo json_encode(array( 
                'response' => array(
                    'success' => false,
                    'message' => 'Based on the email, you have already subscribed to the magazine.'
                ),
                'user' => $alreadyExists ));
        }  

    }

    public function exists($type, $param){
        global $wpdb;

        $query = 'SELECT *
            FROM ' . $this->table . 
            ' WHERE `'. $type .'` = "%1$s";';

        $alreadyExists = $wpdb->get_results($wpdb->prepare( $query, $param ));

        if(count($alreadyExists) > 0){
            return $alreadyExists;
        } else {
            return false;
        } 
    }
} 