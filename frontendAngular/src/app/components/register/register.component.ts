import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from './register.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  Colors: any = ['Red', 'Orange', 'Yellow', 'Green', 'Blue', 'Indigo', 'Violet'];
  message = '';
  
  constructor(private regService: RegisterService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      color: []
    });
  }

  register(form) {
    this.submitted = true;
    this.message = '';
 
    // stop the process here if form is invalid
    if (form.invalid) {
        return;
    } else {
      this.regService.registerColor(form.value).subscribe((res)=>{
        console.log(res);
        
        if(!res.response.success){
          this.message = res.response.message;
        } else {
          this.message = 'Thanks for Subscribing!';
          
          form.resetForm();
          this.submitted = false;
        }
        // this.router.navigateByUrl('home');
      });
    }

    
  }

}
