# Wordpress Plugin with Angular 8 as the frontend

This project is a work in progress.

I built this boilerplate for future plugins where Wordpress handles the backend and Angular is used for the frontend.  Angular is contained within a path, in this case '/boilerplate/', so it will not interfere with any pages built with Wordpress.  This set up is best for any repeating page where the information can be pulled from a database to dynamically populate the page.  For example, product pages, user pages, etc.

To see a live example on my site [click here](https://klworkman.com/boilerplate/).

This boilerplate:

* Creates a new route for the pages using Angular - '/boilerplate/'
* Adds a page with a simple form using Angular - '/boilerplate/subscribe'
* Adds a Table to the Wordpress database to hold the Form submissions
* Pulls data from database for each subscriber based on url parameters - '/boilerplate/subscriber/1'
* uses Gulp to build the final  `/angular-plugin-boilerplate` folder that is ready for upload to the Wordpress Site

## Quickstart guide

* Clone or download this Git repo onto your computer.
* Install [Node.js](https://nodejs.org/en/) if you don't have it yet.
* Run `npm install`
* Run `cd frontendAngular/`
* Run `npm install`
* Run `npm install -g @angular/cli` to install the Angular command line interface
* Run `ng build` to compile the Angluar app
 
## Production build

* From the plugin's base folder, run `gulp switchToProd`.  This will update file references for production.
* Run `cd frontendAngular/`
* Run `ng build` to compile the Angluar app
* Run `cd ../`
* Run `gulp build`. This creates the final build for the plugin, the folder is called "angular-boilerplate-plugin" and it will be located inside the base folder.
* Add this folder to your Wordpress plugins folder and activate.