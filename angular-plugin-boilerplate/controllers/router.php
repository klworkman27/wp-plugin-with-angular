<?php

require_once("../../../../wp-load.php");
require_once('../app/php/classes/subscribers.php'); 

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$type = $request->type;
$data = $request->data;

$subscribers = new Subscribers();

switch ($type) {
    case 'registerColor':
        $subscribers->add($data);
        break;

    case 'getSubscriber':
        $subscriber = $subscribers->exists('id', $data);

        if(count($subscriber) > 0){
            echo json_encode(array( 
                'response' => array(
                    'success' => true,
                    'message' => 'subscriber found'
                ),
                'subscriber' => $subscriber[0] ));

        } else {
            echo json_encode(array( 
                'response' => array(
                    'success' => false,
                    'message' => 'no subscriber found'
                ),
                'subscriber' => $subscriber ));

        }
        
        break;
    
    default:
        echo json_encode(array( 'user' => 
            array(
                'id' => '1',
                'name' => 'Jane Doe',
                'email' => 'jane@gmail.com',
                'color' => 'red',
                'date_created' => date("Y-m-d H:i:s")
            )
        ));
        break;
}

?>