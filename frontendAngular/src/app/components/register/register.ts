export interface Register {
    name: string;
    email: string;
    color: string;
}
