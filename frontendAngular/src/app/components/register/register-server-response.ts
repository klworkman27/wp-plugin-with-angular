export interface RegisterServerResponse {
    response : {
        success : boolean;
        message : string;
    };
    user : {
        id: number;
        name: string;
        email: string;
        color: string;
        date_created: Date;

    }
    
}
