import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Register } from  './register';
import { RegisterServerResponse } from  './register-server-response';
import { tap } from  'rxjs/operators';
import { Observable, BehaviorSubject } from  'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  // AUTH_SERVER = "http://localhost:3000";
  AUTH_SERVER = "../controllers";

  authSubject  =  new  BehaviorSubject(false);

  constructor(private httpClient: HttpClient) {   
  }

  registerColor(reg: Register): Observable<RegisterServerResponse> {
    return this.httpClient.post(`${this.AUTH_SERVER}/router.php`, {type:'registerColor', data: reg}).pipe(
      tap(async (res: RegisterServerResponse) => {
        if (res.user) {
          // localStorage.setItem("ACCESS_TOKEN", res.user.access_token);
          // localStorage.setItem("EXPIRES_IN", res.user.expires_in);
          this.authSubject.next(true);
        }
      })
    );
  } 

}
