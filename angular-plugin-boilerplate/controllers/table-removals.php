<?php

function apb_remove_all_tables(){
    global $wpdb;
    $tables = [
        'angular_boilerplate'
    ];

    foreach($tables as $table_name_ending){
        $table_name = $wpdb->prefix . $table_name_ending;
        $sql = "DROP TABLE IF EXISTS $table_name";
        $wpdb->query($sql);
        delete_option("my_plugin_db_version");
    }
     
}