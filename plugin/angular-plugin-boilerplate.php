<?php
/**
 * Plugin Name: Angular Plugin Boilerplate
 * Description: Base project for a Gulp/Angular/Wordpress plugin project.
 * Author: Kristen Workman
 * Version: 0.0.1
 * Author URI: 	https://klworkman.com
*/

define( 'PLUGIN_WITH_CLASSES__FILE__', __FILE__ );

require_once( ABSPATH . 'wp-content/plugins/angular-plugin-boilerplate/app/php/classes/database.php' );

class angularBoilerplateApp
{	
	public $plugin_dir;
	public $plugin_url;
	public $base_href;
	public $api_route;
	public $versions;


	/**
	 * Setup plugin
	 */
	public function __construct() {

		// General
        $this->plugin_dir = plugin_dir_path( __FILE__ );
        $this->plugin_url = plugins_url( '/', __FILE__ );
		$this->versions = array();

		// Routing
		// $this->api_route = '^api/weather/(.*)/?'; // Matches /api/weather/{position}
		$this->base_href = '/boilerplate/';

		add_filter( 'do_parse_request', array( $this, 'intercept_wp_router' ), 1, 3 );
		add_filter( 'rewrite_rules_array', array( $this, 'rewrite_rules' ) );
		add_filter( 'query_vars', array( $this, 'query_vars' ) );
        add_action( 'wp_loaded', array( $this, 'flush_rewrites' ) );
        
        // Database - register activation and deactivation hooks
        $boilDatabase = new BoilerDatabase();
        $boilDatabase->activation_hook();
        $boilDatabase->deactivation_hook();
	}

	// flush_rules() if our rules are not yet included
	public function flush_rewrites() {
		$rules = get_option( 'rewrite_rules' );

		if ( ! isset( $rules[ $this->api_route ] ) ) {
			global $wp_rewrite;
			$wp_rewrite->flush_rules();
		}
	}

	// Add rule for /api/weather/{position}
	public function rewrite_rules( $rules ) {
		$rules[ $this->api_route ] = 'index.php?api_position=$matches[1]';
		return $rules;
	}

	// Adding the id var so that WP recognizes it
	public function query_vars( $vars ) {
		array_push( $vars, 'api_position' );
		return $vars;
	}

	/**
	 * Intercept WP Router
	 *
	 * Intercept WordPress rewrites and serve a 
	 * static HTML page for our angular.js app.
	 */
	public function intercept_wp_router( $continue, WP $wp, $extra_query_vars ) {

		// Conditions for url path
		$url_match = ( substr( $_SERVER['REQUEST_URI'], 0, strlen( $this->base_href ) ) === $this->base_href );
		if ( ! $url_match ) 
			return $continue;

		// Vars for index view
		$plugin_url = $this->plugin_url;
		$base_href = $this->base_href;

		$temp_title = explode("/", $_SERVER['REQUEST_URI']);
		$page_title = 'Angular Plugin Boilerplate';

		// Browser caching for our main template
		$ttl = DAY_IN_SECONDS;
		header( "Cache-Control: public, max-age=$ttl" );

		// Load index view
		include_once( $this->plugin_dir . 'frontendAngular/dist/frontendAngular/index.html' );
		exit;
	}

} // class ngApp

new angularBoilerplateApp();