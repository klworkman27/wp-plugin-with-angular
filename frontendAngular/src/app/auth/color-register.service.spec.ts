import { TestBed } from '@angular/core/testing';

import { ColorRegisterService } from './color-register.service';

describe('ColorRegisterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ColorRegisterService = TestBed.get(ColorRegisterService);
    expect(service).toBeTruthy();
  });
});
