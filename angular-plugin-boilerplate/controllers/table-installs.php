<?php

function apb_install_all_tables(){
    apb_install();
}

function apb_install () {
    global $wpdb;
    // creates form data table in database if not exists
    $table = $wpdb->prefix . "angular_boilerplate"; 
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE IF NOT EXISTS $table (
        `id` mediumint(9) NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(100) NOT NULL,
        `email` TEXT NOT NULL,
        `date_added` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        UNIQUE (`id`)
        ) $charset_collate;";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}