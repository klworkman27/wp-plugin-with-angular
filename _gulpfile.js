// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const inject = require('gulp-inject');
var replace = require('gulp-replace');
var vendor_files = ['./node_modules/angular/angular.js'];


// File paths
const files = { 
    scssPath: 'app/scss/**/*.scss',
    jsPath: 'app/js/**/*.js'
}

// Sass task: compiles the style.scss file into style.css
function scssTask(){    
    return src(files.scssPath)
        .pipe(sourcemaps.init()) // initialize sourcemaps first
        .pipe(sass()) // compile SCSS to CSS
        .pipe(postcss([ autoprefixer(), cssnano() ])) // PostCSS plugins
        .pipe(sourcemaps.write('.')) // write sourcemaps file in current directory
        .pipe(dest('app/css')
    ); // put final CSS in dist folder
}

// JS task: concatenates and uglifies JS files to script.js
function jsTask(){
    return src([
        files.jsPath, '!app/js/all.js'
        //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
        ])
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(dest('app/js')
    );
}

function copyVendor(done) {
    return src(vendor_files, {base: './node_modules'})
        .pipe(dest('vendor')
    );
}

// Cachebust
var cbString = new Date().getTime();
function cacheBustTask(){
    var sources = src(vendor_files, {read: false});
    return src(['app/views/index.php'])
        .pipe(replace(/cb=\d+/g, 'cb=' + cbString))
        .pipe(inject(sources, {name: 'app', ignorePath: 'node_modules', addPrefix: 'vendor' }))
        .pipe(dest('app/views'));
}

// Watch task: watch SCSS and JS files for changes
// If any change, run scss and js tasks simultaneously
function watchTask(){
    watch([files.scssPath, files.jsPath, '!app/js/all.js'], 
        series(
            parallel(scssTask, jsTask),
            cacheBustTask
        )
    );
}
// functions to move files to production
function moveVendorToProdTask(){
    return src(vendor_files, {base: './node_modules'})
        .pipe(dest('plugin/vendor')
    );
}

function moveAppToProdTask(){
    return src(['app/js/all.js', 
                'app/css/**/*.*',
                'app/php/**/*.*',
                'app/views/**/*.*'], 
                { base: './' })
        .pipe(dest('plugin')
    );
}

function movePluginFilesToProdTask(){
    return src(['angular-plugin-boilerplate.php'], { base: './' })
        .pipe(dest('plugin')
    );
}
// end of moving to productions functions

// Export the default Gulp task so it can be run
// Runs the scss and js tasks simultaneously
// then runs cacheBust, then watch task
exports.default = series(
    parallel(scssTask, jsTask, copyVendor), 
    cacheBustTask,
    watchTask
);


// Exports the build Gulp task.  
// Runs usual tasks then moves all the files to the production folder
exports.build = series(
    parallel(scssTask, jsTask), 
    cacheBustTask,
    parallel(moveVendorToProdTask, moveAppToProdTask, movePluginFilesToProdTask)
);