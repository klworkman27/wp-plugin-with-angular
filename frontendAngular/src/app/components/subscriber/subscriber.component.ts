import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubscriberService } from './subscriber.service';

@Component({
  selector: 'app-subscriber',
  templateUrl: './subscriber.component.html',
  styleUrls: ['./subscriber.component.scss']
})
export class SubscriberComponent implements OnInit {

  id = '';
  subscriber;

  constructor(
    private route: ActivatedRoute, 
    private subService: SubscriberService
    ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      // get the username out of the route params
      this.id = params['id'];

      this.subService
          .getSubscriber(this.id)
          .subscribe(response => {
            console.log(response);
            this.subscriber = response.subscriber;
          });
    })
  }

}
