export interface SubscriberResponse {
    response : {
        success : boolean;
        message : string;
    };
    subscriber : {
        id: number;
        name: string;
        email: string;
        color: string;
        date_created: Date;

    }
}
