import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Subscriber } from  './subscriber';
import { SubscriberResponse } from  './subscriber-response';
import { tap } from  'rxjs/operators';
import { Observable, BehaviorSubject } from  'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SubscriberService {
  AUTH_SERVER = "../controllers";

  authSubject  =  new  BehaviorSubject(false);

  constructor(private http: HttpClient) { }

  getSubscriber(id: string): Observable<SubscriberResponse> {
    return this.http.post(`${this.AUTH_SERVER}/router.php`, {type:'getSubscriber', data: id}).pipe(
      tap(async (sr: SubscriberResponse) => {
        if (sr.subscriber) {
          this.authSubject.next(true);
        }
      })
    );
  } 

}

