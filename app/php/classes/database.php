<?php

class BoilerDatabase
{	
    public $tables;

	/**
	 * Setup plugin
	 */
	public function __construct() {
        $this->tables = [
            'angular_boilerplate'
        ]; 
    }

    public function activation_hook(){
        register_activation_hook( PLUGIN_WITH_CLASSES__FILE__, array( $this, 'apb_install_all_tables' ) );
    }

    public function deactivation_hook(){
        register_deactivation_hook(PLUGIN_WITH_CLASSES__FILE__, array( $this, 'apb_remove_all_tables' ));
    }

    
    public function apb_install_all_tables(){
        $this->apb_install();
    }

    public function apb_remove_all_tables(){
        global $wpdb;
        foreach($this->tables as $table_name_ending){
            $table_name = $wpdb->prefix . $table_name_ending;
            $sql = "DROP TABLE IF EXISTS $table_name";
            $wpdb->query($sql);
            delete_option("my_plugin_db_version");
        }   
    }
    
    private function apb_install () {
        global $wpdb;
        // creates form data table in database if not exists
        $table = $wpdb->prefix . "angular_boilerplate"; 
        $charset_collate = $wpdb->get_charset_collate();
        $sql = "CREATE TABLE IF NOT EXISTS $table (
            `id` mediumint(9) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(100) NOT NULL,
            `email` TEXT NOT NULL,
            `color` TEXT NOT NULL,
            `date_added` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            UNIQUE (`id`)
            ) $charset_collate;";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

} 